default{
    state_entry(){llSetTimerEvent(0.1);}
    timer(){
        vector vR=llRot2Fwd(llGetRot());
        float fX=vR.x;
        float fY=vR.y;
        float fHdg;
        integer iHdg;
        if(fX>0){
            if(fY>0){
                // 0-90
                fHdg=fX*90;
            }
            if(fY<0){
                // 90-180
                fHdg=((fY*-1)*90)+90;
            }
        }
        if(fX<0){
            if(fY<0){
                // 180-270
                fHdg=((fX*-1)*90)+180;
            }
            if(fY>0){
                // 270-360
                fHdg=(fY*90)+270;
            }
        }
        iHdg=llRound(fHdg);
        if(iHdg==360){iHdg=0;}
        llSetText((string)iHdg, <1,1,1>, 1.0);
    }
}