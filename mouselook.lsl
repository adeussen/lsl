vector gvDisabledPrimSize= <0.01, 0.01, 0.01>; // Size for prims not in use
float gFOV = 1.7320508075688774; // precalculated stuff for perspective, FOV is 60 degrees, gFOV is 1./ llTan(((60. * DEG_TO_RAD) / 2.))
vector gOffScreen = <-1000.0, -1000.0, -1000.0>; // an "invalid" return value for Region2Hud
integer giFace=4; // Face of crosshair prim for texture
float gfInterval=0.2; // Interval to scan

float gfRange=100.0; // Range to scan for agents
integer giShowNames=TRUE; // Display nametags above crosshair
vector gvNametagColor=<1,1,1>; // Nametag text color
float gfNametagAlpha=1; // Nametag text alpha
float gfCrosshairAlpha=0.5; // Crosshair alpha
vector gvCrosshairSize=<0.1, 0.1, 0.1>; // Crosshair size
vector gvCrosshairOffset=<0,0,0>; // Offset
key gkCrosshairTexture="5b4d6c7d-440d-6855-e24d-70a4a5cb2609"; //Crosshair texture

list glAgents; // List of agents found in sim
list glAgentPos; // Position of agents
list glAgentDistance; // Distance of agents
integer giLastNumDetected; // Counter
integer giNCLines; // Counter
list glNCLines; // Raw notecard dump
integer giNC=-1; // Counter
key gkReq; // Request handle

vector Region2HUD(vector objectPos, vector cameraPos, rotation cameraRot){
    // Translate object in to camera space.
    objectPos = (objectPos - cameraPos)
    // Rotate object in camera space.
    * (ZERO_ROTATION / cameraRot);
    // Switch axes from Z = up to RHS (X = left, Y = up, Z = forward)
    objectPos = <-objectPos.y, objectPos.z, objectPos.x>;
    // Apply perspective distortion and clip object to HUD
    float xHUD = (objectPos.x * gFOV) / objectPos.z;
    // remove xHUD check, or expand (say, to > -3 && < 3) for a non-square target field spanning the window
    //if (xHUD > -1.0 && xHUD < 1.0)
    if (xHUD > -3.0 && xHUD < 3.0){
        float yHUD = (objectPos.y * gFOV) / objectPos.z;
        if (yHUD > -1.0 && yHUD < 1.0){
            // Set front clipping plane to 1m and back clipping plane to infinity.
            float zHUD = (objectPos.z - 2) / objectPos.z;
            if (zHUD > -1.0 && zHUD < 1.0)
                return <0.0, -xHUD / 2.0, yHUD / 2.0>;
        }
    }
    return gOffScreen;
}
ResetChildPositions(integer startPrim, integer endPrim){
    for ( ; endPrim >= startPrim ; endPrim--){
        llSetLinkPrimitiveParamsFast(endPrim,[
            PRIM_COLOR,ALL_SIDES,<1,1,1>,0.0,
            PRIM_POSITION,<gvDisabledPrimSize.x * endPrim,0,0>,
            PRIM_SIZE,gvDisabledPrimSize,
            PRIM_FULLBRIGHT,ALL_SIDES,FALSE,
            PRIM_TEXT,"",ZERO_VECTOR,0.0
        ]);
    }
}
Setup(){
    ResetChildPositions(2, llGetNumberOfPrims());
    integer attachPoint = llGetAttached();
    if (attachPoint != ATTACH_HUD_CENTER_1 && attachPoint != ATTACH_HUD_CENTER_2){
        llOwnerSay("Attach me to the Center or Center 2 HUD position.");
        llSetText ("-----\n|\n|\nV", <1.0,1.0,0.0>, 1.0);
        return;
    }
    llSetRot(ZERO_ROTATION);
    llSetPos(<0,0,-0.45>); // Home position for root prim
    llSetScale(gvDisabledPrimSize);
    llSetText("", ZERO_VECTOR, 0.0);
    llSetAlpha(0.0,ALL_SIDES);
    llRequestPermissions(llGetOwner(), PERMISSION_TRACK_CAMERA|PERMISSION_TAKE_CONTROLS);
}
default{
    state_entry(){
        gkReq=llGetNumberOfNotecardLines("Mouselook");
    }
    dataserver(key req,string mess){if(req==gkReq){
        if(giNC==-1){
            giNCLines=(integer)mess;
            gkReq=llGetNotecardLine("Mouselook",0);
        }
        else{
            if(mess){if(llGetSubString(mess,0,1)!="//"){
                glNCLines=glNCLines+[mess];            
            }}
        }
        giNC++;
        if(mess!=EOF){gkReq=llGetNotecardLine("Mouselook",giNC);}
        else{
            gfRange=llList2Float(glNCLines,0);
            giShowNames=llList2Integer(glNCLines,1);
            list lTL=llParseString2List(llList2String(glNCLines,2),["<",",",">"],[]);
            float f1=llList2Float(lTL,0);
            float f2=llList2Float(lTL,1);
            float f3=llList2Float(lTL,2);
            gvNametagColor=<f1,f2,f3>;
            gfNametagAlpha=llList2Float(glNCLines,3);
            gfCrosshairAlpha=llList2Float(glNCLines,4);
            lTL=llParseString2List(llList2String(glNCLines,5),["<",",",">"],[]);
            f1=llList2Float(lTL,0);
            f2=llList2Float(lTL,1);
            f3=llList2Float(lTL,2);
            gvCrosshairSize=<f1,f2,f3>;
            lTL=llParseString2List(llList2String(glNCLines,6),["<",",",">"],[]);
            f1=llList2Float(lTL,0);
            f2=llList2Float(lTL,1);
            f3=llList2Float(lTL,2);
            gvCrosshairOffset=<f1,f2,f3>;
            gkCrosshairTexture=llList2Key(glNCLines,7);
            state paused;
        }
    }}
}
state paused{
    state_entry(){Setup();}
    link_message(integer link, integer num, string mess, key id){if(mess=="mlstart"){state running;}}
    run_time_permissions(integer p){
        if (p & PERMISSION_TAKE_CONTROLS){llTakeControls(CONTROL_LBUTTON, TRUE, TRUE);} // keep it running
        if (p & PERMISSION_TRACK_CAMERA){llSetTimerEvent(gfInterval);}
    }
    timer(){
        if(llGetAgentInfo(llGetOwner())&AGENT_MOUSELOOK){state running;}
    }
}
state running{
    state_entry(){Setup();}
    attach(key id){if(id){Setup();}}
    on_rez(integer p){Setup();}
    run_time_permissions(integer p){
        if (p & PERMISSION_TAKE_CONTROLS){llTakeControls(CONTROL_LBUTTON, TRUE, TRUE);} // keep it running
        if (p & PERMISSION_TRACK_CAMERA){llSetTimerEvent(gfInterval);}
    }
    link_message(integer link, integer num, string mess, key id){if(mess=="mlstop"){state paused;}}
    timer(){
        integer i;
        glAgents=[];
        glAgents=llGetAgentList(AGENT_LIST_REGION,[]);
        integer iOwner=llListFindList(glAgents,[llGetOwner()]);
        glAgents=llDeleteSubList(glAgents,iOwner,iOwner);
        //llOwnerSay(llList2CSV(glAgents));
        if(llGetAgentInfo(llGetOwner())&AGENT_MOUSELOOK){
            for(i=0;i<llGetListLength(glAgents);i++){
                key kAgent=llList2Key(glAgents,i);
                vector cameraPos = llGetCameraPos();
                rotation cameraRot = llGetCameraRot();
                vector myPos = llGetPos();
                vector dPos = llList2Vector(llGetObjectDetails(kAgent,[OBJECT_POS]),0);
                float dist = llVecDist(myPos, dPos);
                vector childPos;
                childPos=Region2HUD(dPos + gvCrosshairOffset, cameraPos, cameraRot);
                if (childPos != gOffScreen){ // on screen
                    string tName=llGetDisplayName(kAgent);
                    list nameParts=llParseString2List(tName,[" "],[]);
                    if(llList2String(nameParts,1)=="Resident"){tName=llList2String(nameParts,0);}
                    vector vColor=llVecNorm(<dist,gfRange-dist,0.0>);
                    if(giShowNames==FALSE){tName="";}
                    if(dist<=gfRange){
                        llSetLinkPrimitiveParamsFast(i+2,[
                            PRIM_POSITION,childPos-llGetLocalPos(),
                            PRIM_TEXTURE,giFace,gkCrosshairTexture,<1, 1, 0>,ZERO_VECTOR,0,
                            PRIM_COLOR,ALL_SIDES,vColor,gfCrosshairAlpha,
                            PRIM_SIZE,gvCrosshairSize,
                            PRIM_FULLBRIGHT,ALL_SIDES,TRUE,
                            PRIM_TEXT,tName,gvNametagColor,gfNametagAlpha
                        ]);
                    }
                }
                else{ // off screen
                    llSetLinkPrimitiveParamsFast(i+2,[
                        PRIM_COLOR,ALL_SIDES,<1,1,1>,0.0,
                        PRIM_TEXTURE,giFace,TEXTURE_BLANK,<1, 1, 0>,ZERO_VECTOR,0,
                        PRIM_POSITION,<gvDisabledPrimSize.x*i,0,0>,
                        PRIM_SIZE,gvDisabledPrimSize,
                        PRIM_FULLBRIGHT,ALL_SIDES,TRUE,
                        PRIM_TEXT,"",ZERO_VECTOR,0.0
                    ]);
                }
                if (giLastNumDetected > llGetListLength(glAgents)){
                    ResetChildPositions(i + 2, giLastNumDetected + 1);
                    giLastNumDetected = llGetListLength(glAgents);
                }
            } // end loop through agents
        } // end if mouselook
        else{state paused;}
    } // end timer
}