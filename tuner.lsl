integer giChan=664664; // Channel for dialog menu
float gfTimeout=60.0; // Timeout for dialog menu
string gsNotecard="Stations"; // Name of notecard with format <full name>|<button label>|<stream URL>
integer giButtonsPerPage=10; // Buttons per page of dialog menu (10 max)
//
integer giNotecardLines; // Number of lines in notecard
integer giCurrentLine; // Current notecard line to parse
list glNames; // List of full station names
list glButtons; // List of dialog menu button labels
list glURLs; // List of stream URLs (port numbers only, assume http://listen.livestreamingservice.com:)
integer giPage; // Page number of dialog menu
key gkReq; // Dataserver request key
key gkUser;
integer i;
integer listen0;
integer DEBUG=0;
//
debug2(string mess){if(DEBUG>0){llOwnerSay("DEBUG: "+mess);}}
dialogpage(key avatar,string mess){
    list lB=llList2List(glButtons,(giPage*giButtonsPerPage),((giPage*giButtonsPerPage)+giButtonsPerPage));
    if((giPage*(giButtonsPerPage))>llGetListLength(glButtons)){
        giPage--;
        list lB=llList2List(glButtons,0,giButtonsPerPage);
 
    }
    lB=["<< Back","Next >>"]+llList2List(lB,0,-1);
    llDialog(avatar,mess,lB,giChan);
}
//
default{
    state_entry(){
        giButtonsPerPage=giButtonsPerPage-1; // Subtract buttons per page by 1 since buttons start at zero
        llSetText("Loading...",<0.8,0.8,0.8>,0.8);
        gkReq=llGetNumberOfNotecardLines(gsNotecard);
    }
    dataserver(key id, string mess){if(id==gkReq){
        giNotecardLines=(integer)mess;
        debug2("Notecard lines: "+(string)giNotecardLines);
        state load;
    }}
}
state load{
    state_entry(){
        giCurrentLine=0;
        gkReq=llGetNotecardLine(gsNotecard,giCurrentLine);
    }
    dataserver(key id, string mess){if(id==gkReq){
        debug2("Notecard line #"+(string)giCurrentLine+": "+mess);
        llSetText("Line #"+(string)giCurrentLine,<0.8,0.8,0.8>,0.8);
        list lLine=llParseString2List(mess,["|"],[]);
        glNames=glNames+[llList2String(lLine,0)];
        glButtons=glButtons+[llList2String(lLine,1)];
        glURLs=glURLs+[llList2String(lLine,2)];
        giCurrentLine++;
        if(giCurrentLine<giNotecardLines){
            gkReq=llGetNotecardLine(gsNotecard,giCurrentLine);
        }
        else{state running;}
    }}
}
state running{
    state_entry(){
        llSetText("Pick a station",<0.8,0.8,0.8>,0.8);
        debug2("Names: "+llList2CSV(glNames));
        debug2("Buttons: "+llList2CSV(glButtons));
        debug2("URLs: "+llList2CSV(glURLs));
    }
    touch_start(integer num){
        llSetTimerEvent(gfTimeout);
        listen0=llListen(giChan,"","","");
        gkUser=llDetectedKey(0);
        giPage=0;
        dialogpage(gkUser,"Pick a station...");
    }
    timer(){llListenRemove(listen0);llSetTimerEvent(0);}
    listen(integer chan,string name,key id,string mess){
        debug2("Dialog choice: "+mess);
        if(mess=="<< Back"){giPage--;dialogpage(gkUser,"Pick a station...");return;}
        if(mess=="Next >>"){giPage++;dialogpage(gkUser,"Pick a station...");return;}
        else{
            integer iIndex=0;
            iIndex=llListFindList(glButtons,[mess]);
            debug2("iIndex: "+(string)iIndex);
            debug2("===STATION URL: http://listen.livestreamingservice.com:"+(string)llList2String(glURLs,iIndex));
            llSetParcelMusicURL("http://listen.livestreamingservice.com:"+(string)llList2String(glURLs,iIndex));
            llSetText((string)llList2String(glNames,iIndex),<0.8,0.8,0.8>,0.8);
        }
        giPage=0;
        gkUser="";
        llSetTimerEvent(0);
        llListenRemove(listen0);
    }
}