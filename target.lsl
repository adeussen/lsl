float gfRange=96;
float gfSensorInterval = 0.1;

float gfRatio=6.0; // Ratio of X axis only mode, 1.0 to disable
float gfZOffset=-0.45; // offset to display at bottom of screen
float gf2DZOffset=0.0; // offset on Z axis in 2D mode

float gfMainAlpha=0.5; // Alpha for main prim
vector gvMainColor=<0.5,0.5,0.5>;
float gfMainHeight=1.5; // Multiplies beacon height to get main prim height
integer giFace=4;

vector gvBeaconSize=<0.01,0.01,0.0175>; // 0,width,height

key gkBeaconTexture="e2805707-c266-4cba-837d-c09ec0f63305";
vector gvBeaconColorNear=<0,1,0>;
vector gvBeaconColorFar=<1,0,0>;
float gfBeaconAlphaNear=1;
float gfBeaconAlphaFar=0.25;

vector gvCrosshairSize=<0.075,0.075,0.075>;
key gkCrosshairTexture="084fe18b-e5ae-4cdb-bbed-cbc3eb0b7ecc";
vector gvCrosshairColorNear=<0,1,0>;
vector gvCrosshairColorFar=<1,0,0>;
float gfCrosshairAlphaNear=1;
float gfCrosshairAlphaFar=0.25;

integer giAutoML=TRUE; // Enable 2D mode and names in mouselook
integer giType=SCRIPTED; // Type to scan for
string gsTgtName=""; // Name of target
integer giShowNames=TRUE; // Display names

integer giChan=292992; // Dialog channel
float gfTimeout=30.0; // Dialog menu timeout
list glMenu=["Active","Passive","Scripted","Agent","Name","ML Off","2d On","2D Off","ML On","Names On","Names Off","-Reset-"];
integer giPrv=11; // Private channel for text input


float gFOV = 1.7320508075688774; // precalculated stuff for perspective, FOV is 60 degrees, gFOV is 1./ llTan(((60. * DEG_TO_RAD) / 2.))
vector gOffScreen = <-1000.0, -1000.0, -1000.0>; // an "invalid" return value for Region2Hud
//integer giLastNumDetected; // Counter
list glAgents;
list glPos;
integer gi2D;
integer giLastDetected;
integer giTimer;
integer listen0;
integer listen1;
integer listen2;

vector Region2HUD(vector objectPos, vector cameraPos, rotation cameraRot){
    // Translate object in to camera space.
    objectPos = (objectPos - cameraPos)
    // Rotate object in camera space.
    * (ZERO_ROTATION / cameraRot);
    // Switch axes from Z = up to RHS (X = left, Y = up, Z = forward)
    objectPos = <-objectPos.y, objectPos.z, objectPos.x>;
    // Apply perspective distortion and clip object to HUD
    float xHUD = (objectPos.x * gFOV) / objectPos.z;
    // remove xHUD check, or expand (say, to > -3 && < 3) for a non-square target field spanning the window
    if (xHUD > -1.9 && xHUD < 1.9){ // Original was 3.0
        float yHUD = (objectPos.y * gFOV) / objectPos.z;
        //if (yHUD > -1.0 && yHUD < 1.0){ /////////////////
            float zHUD = (objectPos.z - 2) / objectPos.z;
            if (zHUD > -1.0 && zHUD < 1.0){
                return <0.0, -xHUD / 2.0, yHUD / 2.0>;
            }
        //} ////////// Show dot even if above/below FOV
    }
    return gOffScreen;
}
vector Region2HUD2D(vector objectPos, vector cameraPos, rotation cameraRot){
    // Translate object in to camera space.
    objectPos = (objectPos - cameraPos)
    // Rotate object in camera space.
    * (ZERO_ROTATION / cameraRot);
    // Switch axes from Z = up to RHS (X = left, Y = up, Z = forward)
    objectPos = <-objectPos.y, objectPos.z, objectPos.x>;
    // Apply perspective distortion and clip object to HUD
    float xHUD = (objectPos.x * gFOV) / objectPos.z;
    // remove xHUD check, or expand (say, to > -3 && < 3) for a non-square target field spanning the window
    //if (xHUD > -1.0 && xHUD < 1.0)
    if (xHUD > -3.0 && xHUD < 3.0){
        float yHUD = (objectPos.y * gFOV) / objectPos.z;
        if (yHUD > -1.0 && yHUD < 1.0){
            // Set front clipping plane to 1m and back clipping plane to infinity.
            float zHUD = (objectPos.z - 2) / objectPos.z;
            if (zHUD > -1.0 && zHUD < 1.0)
                return <0.0, -xHUD / 2.0, yHUD / 2.0>;
        }
    }
    return gOffScreen;
}
ResetChildPositions(integer startPrim, integer endPrim){
    for ( ; endPrim >= startPrim ; endPrim--){
        llSetLinkPrimitiveParamsFast(endPrim,[
            PRIM_POSITION,ZERO_VECTOR,
            PRIM_SIZE,gvBeaconSize,
            PRIM_TEXTURE,giFace,gkBeaconTexture,<1, 1, 0>,ZERO_VECTOR,0,
            PRIM_COLOR,ALL_SIDES,ZERO_VECTOR,0.0,
            PRIM_FULLBRIGHT,ALL_SIDES,TRUE,
            PRIM_TEXT,"",<1.0,1.0,1.0>,1.
        ]);
    }
}
Setup(){
    integer attachPoint = llGetAttached();
    if (attachPoint != ATTACH_HUD_CENTER_1 && attachPoint != ATTACH_HUD_CENTER_2){
        llOwnerSay("Attach me to the Center or Center 2 HUD position.");
        llSetText ("-----\n|\n|\nV", <1.0,1.0,0.0>, 1.0);
        return;
    }
    integer i;
    llSetPrimitiveParams([
        PRIM_POSITION,<0,0,gfZOffset>,
        PRIM_SIZE,<gvBeaconSize.x*gfMainHeight,1.95/gfRatio,gvBeaconSize.z*gfMainHeight>,
        PRIM_TEXTURE,giFace,TEXTURE_BLANK,<1, 1, 0>,ZERO_VECTOR,0,
        PRIM_COLOR,ALL_SIDES,gvMainColor,gfMainAlpha,
        PRIM_FULLBRIGHT,ALL_SIDES,TRUE,
        PRIM_TEXT,"",ZERO_VECTOR,0.0
    ]);
    for(i=2;i<=llGetNumberOfPrims();i++){
        llSetLinkPrimitiveParamsFast(LINK_ALL_OTHERS,[
            PRIM_POSITION,ZERO_VECTOR,
            PRIM_SIZE,gvBeaconSize,
            PRIM_TEXTURE,ALL_SIDES,TEXTURE_TRANSPARENT,<1, 1, 0>,ZERO_VECTOR,0,
            PRIM_TEXTURE,giFace,gkBeaconTexture,<1, 1, 0>,ZERO_VECTOR,0,
            PRIM_COLOR,ALL_SIDES,ZERO_VECTOR,0.0,
            PRIM_FULLBRIGHT,ALL_SIDES,TRUE,
            PRIM_TEXT,"",<1.0,1.0,1.0>,1.
        ]);
    }
    giTimer=1; // hack to make sure prims are set up before running
}
default{
    state_entry(){
        Setup();
        llSetTimerEvent(1);
    }
    timer(){if(giTimer==TRUE){
        if(giType==AGENT){state agent;}
        else{state object;}
    }}
    attach(key id){if(id){llResetScript();}}
    on_rez(integer p){llResetScript();}
}
state agent{
    state_entry(){
        glAgents=[];
        llRequestPermissions(llGetOwner(), PERMISSION_TRACK_CAMERA|PERMISSION_TAKE_CONTROLS);
        llSetTimerEvent(0.1);
    }
    attach(key id){if(id){llResetScript();}}
    on_rez(integer p){llResetScript();}
    run_time_permissions(integer p){
        if (p & PERMISSION_TAKE_CONTROLS){llTakeControls(CONTROL_LBUTTON, TRUE, TRUE);} // keep it running
        if (p & PERMISSION_TRACK_CAMERA){llSensorRepeat(gsTgtName,"",giType,gfRange,PI,gfSensorInterval);}
    } 
    touch_start(integer num){
        listen0=llListen(giChan,"",llGetOwnerKey(llGetKey()),"");
        llDialog(llDetectedKey(0),"Pick an option:",glMenu,giChan);
        giTimer=0;
    }
    timer(){
        integer i;
        if(glAgents==[]){ResetChildPositions(giLastDetected+2,llGetNumberOfPrims());}
        if(llGetAgentInfo(llGetOwner())&AGENT_MOUSELOOK){llSetAlpha(0.0,ALL_SIDES);}else{llSetAlpha(gfMainAlpha,ALL_SIDES);}
        ////////////
        /////
        glAgents=llGetAgentList(AGENT_LIST_REGION,[]);
        glAgents=llDeleteSubList(glAgents,llListFindList(glAgents,[llGetOwner()]),llListFindList(glAgents,[llGetOwner()]));
        glPos=[];
        integer iA=llGetListLength(glAgents);
        for(i=0;i<=iA;i++){
            glPos=glPos+llGetObjectDetails(llList2Key(glAgents,i),[OBJECT_POS]);
        }
        //llOwnerSay("=Before=");
        //llOwnerSay(llList2CSV(glAgents));
        //llOwnerSay(llList2CSV(glPos));
        for(i=0;i<=iA;i++){
            if(llVecDist(llList2Vector(glPos,i),llGetPos())>gfRange){
                glAgents=llDeleteSubList(glAgents,i,i);
                glPos=llDeleteSubList(glPos,i,i);
            }
        }
        iA=llGetListLength(glAgents);
        for(i=0;i<=iA;i++){
            if(llVecDist(llList2Vector(glPos,i),llGetPos())>gfRange){
                glAgents=llDeleteSubList(glAgents,i,i);
                glPos=llDeleteSubList(glPos,i,i);
            }
        }
        iA=llGetListLength(glAgents);
        //for(i=0;i<iA;i++){
        //    llOwnerSay((string)i+": "+llList2String(llGetObjectDetails(llList2Key(glAgents,i),[OBJECT_NAME]),0)+", "+(string)llVecDist(llList2Vector(glPos,i),llGetPos())+"m.");
        //}
        integer iShowNames;
        if(giAutoML=TRUE){if(llGetAgentInfo(llGetOwner())&AGENT_MOUSELOOK){iShowNames=giShowNames;gi2D=TRUE;}else{iShowNames=FALSE;gi2D=FALSE;}}
        vector cameraPos = llGetCameraPos();
        rotation cameraRot = llGetCameraRot();
        vector myPos = llGetPos();
        for(i=0;i<iA;i++){
            key k=llList2Key(glAgents,i);
            vector dPos = llList2Vector(glPos,i);
            float dist = llVecDist(myPos, dPos);
            vector childPos;
            if(gi2D){childPos=Region2HUD2D(dPos + <0.0, 0.0, gf2DZOffset>, cameraPos, cameraRot);}
            else{childPos=Region2HUD(dPos + <0.0, 0.0, gf2DZOffset>, cameraPos, cameraRot);}
            if (childPos != gOffScreen){ // on screen
                string tName="";
                if(iShowNames==TRUE){
                    tName=llList2String(llGetObjectDetails(k,[OBJECT_NAME]),0);
                    list nameParts=llParseString2List(tName,[" "],[]);
                    if(llList2String(nameParts,1)=="Resident"){tName=llList2String(nameParts,0);}
                    string dispName=llGetDisplayName(k);
                    if(dispName){if(llToLower(dispName)!=llToLower(tName)){tName=dispName+"("+llGetUsername(k)+")";}}
                }
                if(gi2D==FALSE){
                    llSetAlpha(gfMainAlpha,ALL_SIDES);
                    childPos=<0,childPos.y/gfRatio,gfZOffset>;
                    float fCX=(gvBeaconColorNear.x-gvBeaconColorFar.x)*(dist/gfRange);
                    float fCY=(gvBeaconColorNear.y-gvBeaconColorFar.y)*(dist/gfRange);
                    float fCZ=(gvBeaconColorNear.z-gvBeaconColorFar.z)*(dist/gfRange);
                    vector vColor=gvBeaconColorNear-<fCX,fCY,fCZ>;
                    float fA=gfBeaconAlphaNear-((gfBeaconAlphaNear-gfBeaconAlphaFar)*dist/gfRange);
                    llSetLinkPrimitiveParamsFast(i+2,[
                        PRIM_POSITION,childPos-llGetLocalPos(),
                        PRIM_SIZE,gvBeaconSize,
                        PRIM_TEXTURE,giFace,gkBeaconTexture,<1, 1, 0>,ZERO_VECTOR,0,
                        PRIM_COLOR,ALL_SIDES,vColor,fA,
                        PRIM_FULLBRIGHT,ALL_SIDES,TRUE,
                        PRIM_TEXT,tName,<1.0,1.0,1.0>,1.
                    ]);
                }
                if(gi2D==TRUE){
                    llSetAlpha(0.0,ALL_SIDES);
                    float fCX=(gvCrosshairColorNear.x-gvCrosshairColorFar.x)*(dist/gfRange);
                    float fCY=(gvCrosshairColorNear.y-gvCrosshairColorFar.y)*(dist/gfRange);
                    float fCZ=(gvCrosshairColorNear.z-gvCrosshairColorFar.z)*(dist/gfRange);
                    vector vColor=gvCrosshairColorNear-<fCX,fCY,fCZ>;
                    float fA=gfCrosshairAlphaNear-((gfCrosshairAlphaNear-gfCrosshairAlphaFar)*dist/gfRange);
                    llSetLinkPrimitiveParamsFast(i+2,[
                        PRIM_POSITION,childPos-llGetLocalPos(),
                        PRIM_SIZE,gvCrosshairSize,
                        PRIM_TEXTURE,giFace,gkCrosshairTexture,<1, 1, 0>,ZERO_VECTOR,0,
                        PRIM_COLOR,ALL_SIDES,vColor,fA,
                        PRIM_FULLBRIGHT,ALL_SIDES,TRUE,
                        PRIM_TEXT,tName,<1.0,1.0,1.0>,1.
                    ]);
                }            
            }
            else{ // off screen
                ResetChildPositions(i+2,llGetNumberOfPrims());
            }
            giLastDetected=iA;
        }
        ////////////
        ////////////
        giTimer++;
        if(giTimer>=gfTimeout*10){
            if(listen0){llListenRemove(listen0);}
            if(listen1){llListenRemove(listen1);}
            giTimer=0;
        }
    }
    listen(integer chan,string name,key id,string mess){
        if(chan==giChan){
            if(mess=="-Reset-"){llResetScript();}
            if(mess=="2D On"){gi2D=TRUE;llOwnerSay("2D is on.");}
            if(mess=="2D Off"){gi2D=FALSE;llOwnerSay("2D is off.");}
            if(mess=="ML On"){giAutoML=TRUE;gi2D=FALSE;llOwnerSay("2D mouselook is on.");}
            if(mess=="ML Off"){giAutoML=FALSE;gi2D=FALSE;llOwnerSay("2D mouselookis off.");}
            if(mess=="Names On"){giShowNames=TRUE;llOwnerSay("Nametags are on.");}
            if(mess=="Names Off"){giShowNames=FALSE;llOwnerSay("Nametags are off.");}
            if(mess=="Active"){gsTgtName="";giType=ACTIVE;llOwnerSay("Searching for active objects.");}
            if(mess=="Passive"){gsTgtName="";giType=PASSIVE;llOwnerSay("Searching for passive objects.");}
            if(mess=="Scripted"){gsTgtName="";giType=SCRIPTED;llOwnerSay("Searching for scripted objects.");}
            if(mess=="Agent"){gsTgtName="";giType=AGENT;llOwnerSay("Searching for agents.");}
            if(mess=="Name"){
                listen1=llListen(0,"",id,"");
                listen2=llListen(giPrv,"",id,"");
                llOwnerSay("Say the name of the object to search for over public chat or /"+(string)giPrv+".");
                llSetTimerEvent(gfTimeout);
            }
            llListenRemove(listen0);
        }
        if(chan==0){
            gsTgtName=mess;
            llOwnerSay("Now searching for '"+gsTgtName+"'.");
            llListenRemove(listen1);
            llListenRemove(listen2);
        }
        if(chan==giPrv){
            gsTgtName=mess;
            llOwnerSay("Now searching for '"+gsTgtName+"'.");
            llListenRemove(listen1);
            llListenRemove(listen2);
        }
        integer i;
        ResetChildPositions(2,llGetNumberOfPrims());
        if(giType!=AGENT){state object;}
    }  
}
state object{
    state_entry(){
        if(giType==AGENT){state agent;}
        llRequestPermissions(llGetOwner(), PERMISSION_TRACK_CAMERA|PERMISSION_TAKE_CONTROLS);
        llSetTimerEvent(0.1);
    }
    attach(key id){if(id){llResetScript();}}
    on_rez(integer p){llResetScript();}
    run_time_permissions(integer p){
        if (p & PERMISSION_TAKE_CONTROLS){llTakeControls(CONTROL_LBUTTON, TRUE, TRUE);} // keep it running
        if (p & PERMISSION_TRACK_CAMERA){llSensorRepeat(gsTgtName,"",giType,gfRange,PI,gfSensorInterval);}
    } 
    no_sensor(){
        ResetChildPositions(2,llGetNumberOfPrims());
        if(llGetAgentInfo(llGetOwner())&AGENT_MOUSELOOK){llSetAlpha(0.0,ALL_SIDES);}else{llSetAlpha(gfMainAlpha,ALL_SIDES);}
    }
    sensor(integer num){
        integer iShowNames;
        if(giAutoML=TRUE){if(llGetAgentInfo(llGetOwner())&AGENT_MOUSELOOK){iShowNames=giShowNames;gi2D=TRUE;}else{iShowNames=FALSE;gi2D=FALSE;}}
        vector cameraPos = llGetCameraPos();
        rotation cameraRot = llGetCameraRot();
        vector myPos = llGetPos();
        integer i;
        for(i=0;i<num;i++){if(llDetectedKey(i)!=llGetOwner()){
            vector dPos = llDetectedPos(i);
            float dist = llVecDist(myPos, dPos);
            vector childPos;
            if(gi2D){childPos=Region2HUD2D(dPos + <0.0, 0.0, gf2DZOffset>, cameraPos, cameraRot);}
            else{childPos=Region2HUD(dPos + <0.0, 0.0, gfZOffset>, cameraPos, cameraRot);}
            if (childPos != gOffScreen){ // on screen
                string tName="";
                if(iShowNames==TRUE){
                    tName=llDetectedName(i);
                    if(llDetectedType(i)==AGENT){
                        key k=llDetectedKey(i);
                        list nameParts=llParseString2List(tName,[" "],[]);
                        if(llList2String(nameParts,1)=="Resident"){tName=llList2String(nameParts,0);}
                        string dispName=llGetDisplayName(k);
                        if(dispName){if(llToLower(dispName)!=llToLower(tName)){tName=dispName+"("+llGetUsername(k)+")";}}
                    }
                }
                if(gi2D==FALSE){
                    llSetAlpha(gfMainAlpha,ALL_SIDES);
                    float fCX=(gvBeaconColorNear.x-gvBeaconColorFar.x)*(dist/gfRange);
                    float fCY=(gvBeaconColorNear.y-gvBeaconColorFar.y)*(dist/gfRange);
                    float fCZ=(gvBeaconColorNear.z-gvBeaconColorFar.z)*(dist/gfRange);
                    vector vColor=gvBeaconColorNear-<fCX,fCY,fCZ>;
                    float fA=gfBeaconAlphaNear-((gfBeaconAlphaNear-gfBeaconAlphaFar)*dist/gfRange);
                    childPos=<0,childPos.y/gfRatio,gfZOffset>;
                    llSetLinkPrimitiveParamsFast(i+2,[
                        PRIM_POSITION,childPos-llGetLocalPos(),
                        PRIM_SIZE,gvBeaconSize,
                        PRIM_TEXTURE,giFace,gkBeaconTexture,<1, 1, 0>,ZERO_VECTOR,0,
                        PRIM_COLOR,ALL_SIDES,vColor,fA,
                        PRIM_FULLBRIGHT,ALL_SIDES,TRUE,
                        PRIM_TEXT,tName,<1.0,1.0,1.0>,1.
                    ]);
                }
                if(gi2D==TRUE){
                    llSetAlpha(0.0,ALL_SIDES);
                    float fCX=(gvCrosshairColorNear.x-gvCrosshairColorFar.x)*(dist/gfRange);
                    float fCY=(gvCrosshairColorNear.y-gvCrosshairColorFar.y)*(dist/gfRange);
                    float fCZ=(gvCrosshairColorNear.z-gvCrosshairColorFar.z)*(dist/gfRange);
                    vector vColor=gvCrosshairColorNear-<fCX,fCY,fCZ>;
                    float fA=gfCrosshairAlphaNear-((gfCrosshairAlphaNear-gfCrosshairAlphaFar)*dist/gfRange);
                    llSetLinkPrimitiveParamsFast(i+2,[
                        PRIM_POSITION,childPos-llGetLocalPos(),
                        PRIM_SIZE,gvCrosshairSize,
                        PRIM_TEXTURE,giFace,gkCrosshairTexture,<1, 1, 0>,ZERO_VECTOR,0,
                        PRIM_COLOR,ALL_SIDES,vColor,fA,
                        PRIM_FULLBRIGHT,ALL_SIDES,TRUE,
                        PRIM_TEXT,tName,<1.0,1.0,1.0>,1.
                    ]);
                }            
            }
            else{ // off screen
                ResetChildPositions(i+2,llGetNumberOfPrims());
            }
            giLastDetected=num;
        }}
    }
    touch_start(integer num){
        listen0=llListen(giChan,"",llGetOwnerKey(llGetKey()),"");
        llDialog(llDetectedKey(0),"Pick an option:",glMenu,giChan);
        giTimer=0;
    }
    timer(){
        giTimer++;
        integer i;
        ResetChildPositions(giLastDetected+2,llGetNumberOfPrims());
        if(giTimer>=gfTimeout*10){
            if(listen0){llListenRemove(listen0);}
            if(listen1){llListenRemove(listen1);}
            giTimer=0;
        }
    }
    listen(integer chan,string name,key id,string mess){
        if(chan==giChan){
            if(mess=="-Reset-"){llResetScript();}
            if(mess=="2D On"){gi2D=TRUE;llOwnerSay("2D is on.");}
            if(mess=="2D Off"){gi2D=FALSE;llOwnerSay("2D is off.");}
            if(mess=="ML On"){giAutoML=TRUE;gi2D=FALSE;llOwnerSay("2D mouselook is on.");}
            if(mess=="ML Off"){giAutoML=FALSE;gi2D=FALSE;llOwnerSay("2D mouselookis off.");}
            if(mess=="Names On"){giShowNames=TRUE;llOwnerSay("Nametags are on.");}
            if(mess=="Names Off"){giShowNames=FALSE;llOwnerSay("Nametags are off.");}
            if(mess=="Active"){gsTgtName="";giType=ACTIVE;llOwnerSay("Searching for active objects.");}
            if(mess=="Passive"){gsTgtName="";giType=PASSIVE;llOwnerSay("Searching for passive objects.");}
            if(mess=="Scripted"){gsTgtName="";giType=SCRIPTED;llOwnerSay("Searching for scripted objects.");}
            if(mess=="Agent"){gsTgtName="";giType=AGENT;llOwnerSay("Searching for agents.");}
            if(mess=="Name"){
                listen1=llListen(0,"",id,"");
                listen2=llListen(giPrv,"",id,"");
                llOwnerSay("Say the name of the object to search for over public chat or /"+(string)giPrv+".");
                llSetTimerEvent(gfTimeout);
            }
            llListenRemove(listen0);
        }
        if(chan==0){
            gsTgtName=mess;
            llOwnerSay("Now searching for '"+gsTgtName+"'.");
            llListenRemove(listen1);
            llListenRemove(listen2);
        }
        if(chan==giPrv){
            gsTgtName=mess;
            llOwnerSay("Now searching for '"+gsTgtName+"'.");
            llListenRemove(listen1);
            llListenRemove(listen2);
        }
        llSensorRemove();
        integer i;
        ResetChildPositions(2,llGetNumberOfPrims());
        if(giType==AGENT){state agent;}
        llSensorRepeat(gsTgtName,"",giType,gfRange,PI,gfSensorInterval);
    }  
}