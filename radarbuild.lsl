// Targeting HUD builder script v1.0
 
float gPrimSize = 0.1;
key gkTexture="d30f6f49-5370-7845-cfa9-97c420d5304a";
integer giSide=4;
string gWorkPrim;
integer gNewChild;
integer gWorking = FALSE;
 
default
{
    state_entry()
    {
        llSetText("Rez a generic cube, take it to your inventory,\n"
                  + "Ctrl+drag it to inside this prim, then touch to begin", <1, 1, 1>, 1);
    }
 
    touch_start(integer num)
    {
        if (gWorking)
            return;
 
        if (llDetectedKey(0) != llGetOwner())
            return;
 
        gWorkPrim = llGetInventoryName(INVENTORY_OBJECT, 0);
        if (gWorkPrim == "")
        {
            llSetText("No object found in object inventory. Rez a generic cube, take it to your\n"
                      + "inventory, Ctrl+drag it to inside this prim, then touch to try again.", <1, 1, 0>, 1);
            return;
        }
        llRequestPermissions(llGetOwner(), PERMISSION_CHANGE_LINKS);
    }
 
    run_time_permissions(integer perm)
    {
        if (gWorking)
            return;
 
        if (perm & PERMISSION_CHANGE_LINKS)
        {
            gWorking = TRUE;
            llSetText("Working...", <1, 1, 0>, 1);
            llSetLinkPrimitiveParamsFast(LINK_THIS, [
                PRIM_ROTATION,
                    ZERO_ROTATION,
                PRIM_SIZE,
                    <gPrimSize, gPrimSize, gPrimSize>,
                PRIM_TEXTURE,
                    ALL_SIDES,
                    TEXTURE_BLANK,
                    <1, 1, 0>,      // repeats
                    ZERO_VECTOR,    // offsets
                    0,              // rotation in rads
                PRIM_COLOR,
                    ALL_SIDES,
                    <0.5,0.5,0.5>,     // color
                    0.5,             // alpha
                PRIM_FULLBRIGHT,
                    ALL_SIDES,
                    TRUE
            ]);
            gNewChild = 0;
            llRezObject(gWorkPrim, llGetPos() + <.5, 0, 0>, ZERO_VECTOR, ZERO_ROTATION, 0);
        }
    }
 
    object_rez(key id)
    {
        llCreateLink(id, LINK_ROOT);
        llSetLinkPrimitiveParamsFast(2, [
            PRIM_TYPE,
                PRIM_TYPE_BOX,
                    PRIM_HOLE_DEFAULT,  // hole type
                    <0, 1, 0>,          // cut
                    0,                  // hollow
                    <0, 0, 0>,          // twist
                    <1, 1, 0>,          // top size
                    <0, 0, 0>,          // top shear
            PRIM_POSITION,
                <gPrimSize * (gNewChild + 1), 0, 0>,
            PRIM_ROTATION,
                ZERO_ROTATION,
            PRIM_SIZE,
                <gPrimSize, gPrimSize, gPrimSize>,
            PRIM_TEXTURE,
                ALL_SIDES,
                TEXTURE_TRANSPARENT,
                <1, 1, 0>,      // repeats
                ZERO_VECTOR,    // offsets
                0,              // rotation in rads
            PRIM_COLOR,
                ALL_SIDES,
                <1, 1, 1>,      // color
                .0,             // alpha
            PRIM_FULLBRIGHT,
                ALL_SIDES,
                TRUE,
            PRIM_DESC,
                (string)gNewChild
        ]);
        llSetLinkPrimitiveParamsFast(2,[
                PRIM_TEXTURE,
                giSide,
                gkTexture,
                <1, 1, 0>,      // repeats
                ZERO_VECTOR,    // offsets
                0,              // rotation in rads
            PRIM_COLOR,
                ALL_SIDES,
                <1, 1, 1>,      // color
                .5  
            ]);
 
        gNewChild++;
        if (gNewChild < 16)
        {
            llRezObject(gWorkPrim, llGetPos() + <.5, 0, 0>, ZERO_VECTOR, ZERO_ROTATION, 0);
        }
        else
        {
            llRemoveInventory(gWorkPrim);
            llSetText("Object created, you can drag in the radar script now.", <0, 1, 0>, 1);
            llRemoveInventory(llGetScriptName());
        }
    }
}